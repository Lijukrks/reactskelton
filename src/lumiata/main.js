import React from 'react';
var ReactDOM = require('react-dom');
var ReactRouter = require('react-router');
var Router = ReactRouter.Router;
var Route = ReactRouter.Route;
var MainFrame = require("./components/MainFrame");
import { hashHistory,Redirect,IndexRoute} from 'react-router'
ReactDOM.render(
				<Router history={hashHistory} >
			        <Redirect from="/" to="/mainFrame" />
			        <Route path="/mainFrame" component={MainFrame}/>
		        </Router>
					,document.getElementById('renderData')
				);