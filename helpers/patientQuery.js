

function PatientQuery(esURL,index,type,itemField, slop ) {
    this.esURL = esURL;
    this.index = index;
    this.type = type;
    this.itemField = itemField;
    this.slop = slop;
    this.patientConditions = [];
}

PatientQuery.prototype = {
  search: function (callBack) {
   var data = JSON.stringify(this.toString())
   console.log("query...",data)
   var request = require('request');
   request.post({
      headers: {'content-type' : 'application/json'},
      url:     this.esURL+"/"+this.index+"/"+this.type+"/"+"_search",
      body:    data
      }, 
      function(error, response, body){
            if(response){
              // console.log('body.......',body)
              callBack(response)
            }
      });
    },
    
   
  addItems:function(items){
    var clauses = this.appendClauses(items);
    var span_near = {
      fields:[],
      query:{
        span_near : {
          clauses : clauses,
          slop : this.slop,
          in_order : false
        }
      }
    }
    span_near.fields.push(this.itemField)
    this.patientConditions.push(span_near)
  },

  appendClauses:function(items){
    // console.log("item",JSON.stringify(items.conditions,null,4))
    var clauses = []
    for(var i=0 in items.conditions){
      clauses.push(items.conditions[i]);
    }
    return clauses;
  },

  otherAppendClauses:function(itemFirst,itemSecond){
    // console.log("item",JSON.stringify(items.conditions,null,4))
    var clauses = []
    for(var i=0 in itemFirst.conditions){
      clauses.push(itemFirst.conditions[i]);
      clauses.push(itemSecond.conditions[i]) //itemsecond may have different no of conditions
    }
    return clauses;
  },
      
  addOrItems:function(items){
    var clauses = this.appendClauses(items);
    var orClause = {
      query: {
        span_or : {
          clauses : clauses
        }
      }
    }
    this.patientConditions.push(orClause)
  },  

  addIsItemBefore:function(itemFirst,itemSecond){
    var clauses = this.otherAppendClauses(itemFirst,itemSecond)
    var span_near = {
      fields: [],
      query: {
        span_near : {
          clauses : clauses,
          slop : this.slop,
          in_order : true
        }
      }
    }
    span_near.fields.push(this.itemField)
    this.patientConditions.push(span_near)
  },
                                          
  addIsItemAfter:function(itemFirst,itemSecond){
    var clauses = this.otherAppendClauses(itemSecond,itemFirst)
    var span_near = {
      field:[],
      query:{
        span_near : {
          clauses : clauses,
          slop : this.slop,
          in_order : true
        }
      }
    }
    span_near.fields.push(this.itemField)
    this.patientConditions.push(span_near)
  },  

  toString: function(){
      return this.patientConditions[0];
   }

}
    

module.exports= PatientQuery;

  
  /*


  */

  
  



  

