var searchResult = require('../helpers/patientDetailsService')
module.exports = {
    getPatientDetails:getPatientDetails
};
function getPatientDetails(req,res){
	var searchBody = req.swagger.params.patientDetailsBody.value;
	var patientId = searchBody.patientId;
	 searchResult.getPatientEs(patientId,function (error, response){
	    if (error) {
	        console.log(" Search result error occured");
	        res.json({});
	    }
	    else
	    {
	    	console.log("response",response)
	        res.json(response);
	    }
    });
    
}
