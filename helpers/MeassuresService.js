var elasticsearch = require('elasticsearch');
var client = new elasticsearch.Client({
	host: 'http://188.166.241.231:9200/'
});

module.exports = {
	getPatientEs: function (callBack) {
		var queryBody ;
		queryBody = {
		  "size": 0,
		  "aggs": {
		    "messages": {
		      "filters": {
		        "filters": {
		          "Denomiator": {
		            "span_near": {
		              "clauses": [
		                {
		                  "span_term": {
		                    "items": "NODE_TYPE:**DIAGNOSIS**"
		                  }
		                },
		                {
		                  "span_term": {
		                    "items": "SOURCE_CODE:428.0"
		                  }
		                }
		              ],
		              "in_order": false,
		              "slop": 100
		            }
		          },
		          "Numerator": {
		            "span_near": {
		              "clauses": [
		                {
		                  "span_term": {
		                    "items": "NODE_TYPE:**MEDICATION**"
		                  }
		                },
		                {
		                  "span_term": {
		                    "items": "NDC:00378363205"
		                  }
		                }
		              ],
		              "in_order": false,
		              "slop": 100
		            }
		          }
		        }
		      }
		    }
		  }
		}
		client.search({
		index: "lumiata-60k",
		body: queryBody
		}, function (error, response) {
			if (error) {
				callBack(error, null);
			} else {
				callBack(null, response);
			}
		});
	}
}