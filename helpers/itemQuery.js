function ItemQuery(itemField) {
    this.itemField = itemField;
    this.conditions = []
}
    


ItemQuery.prototype = {
   addItemCondition: function(field,value){
   	var obj = {
    		"span_term" : {  }
  	};
	obj.span_term[this.itemField] = field + ":" + value;
	this.conditions.push(obj);
   },


   addItemDateRangeCondition: function(field,from,to){

    if (from== null){ 
     from = 00000000;
   }
    if (to == null){ 
     to = 9999999;
   }
    var obj = {
          "span_multi":{
                "match": {
                    "range":{

                  }
                }
              }
            };
    
    obj.span_multi.match.range[this.itemField]={"gte":field+":"+from,"lte":field+":"+to};
    this.conditions.push(obj);
   },
    
   getObject: function(){
      return this.conditions;
   }
}

module.exports = ItemQuery;


